using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllowedDeniedUiPanel : MonoBehaviour
{
    [SerializeField]
    private GameObject allowedObject;
    [SerializeField]
    private GameObject deniedObject;

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    public void SetAllowed(bool allowed)
    {
        allowedObject.SetActive(allowed);
        deniedObject.SetActive(!allowed);
    }
}
