using System;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ChairTeleportationAnchor : BaseTeleportationInteractable
{
    [SerializeField]
    private GameObject defaultAnchorDrawer;
    [SerializeField]
    private GameObject deniedAnchorDrawer;
    [SerializeField]
    private AllowedDeniedUiPanel allowedDeniedUiPanel;

    [field: SerializeField]
    public Transform TeleportAnchorTransform { get; private set; }

    private Func<bool> TeleportAvailabilityChecker;

    protected override void OnEnable()
    {
        base.OnEnable();
        firstHoverEntered.AddListener(OnFirstHoverEntered);
        lastHoverExited.AddListener(OnLastHoverExited);
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        firstHoverEntered.RemoveListener(OnFirstHoverEntered);
        lastHoverExited.RemoveListener(OnLastHoverExited);
    }


    protected void OnValidate()
    {
        if (TeleportAnchorTransform == null)
            TeleportAnchorTransform = transform;
    }

    protected override void Reset()
    {
        base.Reset();
        TeleportAnchorTransform = transform;
    }

    protected void OnDrawGizmos()
    {
        if (TeleportAnchorTransform == null)
            return;

        Gizmos.color = Color.red;
        GizmoHelpers.DrawWireCubeOriented(TeleportAnchorTransform.position, TeleportAnchorTransform.rotation, 1f);

        GizmoHelpers.DrawAxisArrows(TeleportAnchorTransform, 1f);
    }

    public void Init(Func<bool> teleportAvailabilityChecker)
    {
        TeleportAvailabilityChecker = teleportAvailabilityChecker;
    }

    public void Refresh()
    {
        SetActiveDrawer(allowedDeniedUiPanel.gameObject.activeSelf);
    }

    protected override bool GenerateTeleportRequest(IXRInteractor interactor, RaycastHit raycastHit, ref TeleportRequest teleportRequest)
    {
        if (!IsChairFree() || TeleportAnchorTransform == null)
        {
            return false;
        }

        teleportRequest.destinationPosition = TeleportAnchorTransform.position;
        teleportRequest.destinationRotation = TeleportAnchorTransform.rotation;
        return true;
    }

    private void OnFirstHoverEntered(HoverEnterEventArgs args)
    {
        SetActiveDrawer(true);
    }

    private void OnLastHoverExited(HoverExitEventArgs args)
    {
        SetActiveDrawer(false);
    }

    private void SetActiveDrawer(bool active)
    {
        if (!active)
        {
            allowedDeniedUiPanel.SetActive(false);
            defaultAnchorDrawer.SetActive(false);
            deniedAnchorDrawer.SetActive(false);
        }
        else
        {
            bool isFree = IsChairFree();
            allowedDeniedUiPanel.SetActive(true);
            allowedDeniedUiPanel.SetAllowed(isFree);

            defaultAnchorDrawer.SetActive(isFree);
            deniedAnchorDrawer.SetActive(!isFree);
        }
    }

    private bool IsChairFree()
    {
        if (TeleportAvailabilityChecker != null)
        {
            return TeleportAvailabilityChecker.Invoke();
        }
        return true;
    }
}

