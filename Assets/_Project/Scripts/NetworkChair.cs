using Photon.Pun;
using Photon.Realtime;
using Unity.XR.CoreUtils;
using UnityEngine;

public class NetworkChair : MonoBehaviourPunCallbacks, IPunObservable
{
    private const int FreeChairPlayerId = 0;

    [SerializeField]
    private ChairTeleportationAnchor chairAnchor;
    [SerializeField]
    private int sittingPlayerId;

    private void Awake()
    {
        sittingPlayerId = FreeChairPlayerId;
        chairAnchor.Init(IsChairFree);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.TryGetComponent(out XROrigin playerOrigin))
        {
            return;
        }

        if (!IsChairFree())
        {
            playerOrigin.gameObject.SetActive(false);
            Vector3 freeNearestPosition = transform.position - transform.forward;
            playerOrigin.gameObject.transform.SetPositionAndRotation(freeNearestPosition, transform.rotation);
            playerOrigin.MoveCameraToWorldLocation(new Vector3(playerOrigin.transform.position.x,
                playerOrigin.Camera.transform.position.y, playerOrigin.transform.position.z));
            playerOrigin.MatchOriginUpCameraForward(transform.up, transform.forward);
            playerOrigin.gameObject.SetActive(true);
            return;
        }

        photonView.RequestOwnership();

        SetSittingPlayerId(PhotonNetwork.LocalPlayer.ActorNumber);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<XROrigin>() == null)
        {
            return;
        }
        if (photonView.IsMine)
        {
            SetSittingPlayerId(FreeChairPlayerId);
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);

        if (photonView.IsMine)
        {
            if (sittingPlayerId == otherPlayer.ActorNumber)
            {
                SetSittingPlayerId(FreeChairPlayerId);
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(sittingPlayerId);
        }
        else
        {
            SetSittingPlayerId((int)stream.ReceiveNext());
        }
    }

    private void SetSittingPlayerId(int playerId)
    {
        sittingPlayerId = playerId;
        chairAnchor.Refresh();
    }

    private bool IsChairFree()
    {
        return sittingPlayerId == FreeChairPlayerId;
    }
}
