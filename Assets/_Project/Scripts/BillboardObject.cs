using UnityEngine;

public class BillboardObject : MonoBehaviour
{
    private Transform cameraTransform;

    private void Awake()
    {
        cameraTransform = Camera.main.transform;
    }

    private void Update()
    {
        if (cameraTransform == null)
        {
            return;
        }
        Vector3 newForward = transform.position - cameraTransform.position;
        newForward.y = 0;
        transform.forward = newForward;
    }
}
