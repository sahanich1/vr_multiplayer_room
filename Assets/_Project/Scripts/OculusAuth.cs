using UnityEngine;
using Oculus.Platform;
using Oculus.Platform.Models;
using System;

public class OculusAuth
{
    private Action<string> callback;

    public void LogInUser(Action<string> callback)
    {
        this.callback = callback;
        Core.AsyncInitialize().OnComplete(OnInitializationCallback);
    }

    private void OnInitializationCallback(Message<PlatformInitialize> msg)
    {
        if (msg.IsError)
        {
            Debug.LogErrorFormat("Oculus: Error during initialization. Error Message: {0}",
                msg.GetError().Message);
            PerformCallback();
        }
        else
        {
            Entitlements.IsUserEntitledToApplication().OnComplete(OnIsEntitledCallback);
        }
    }

    private void OnIsEntitledCallback(Message msg)
    {
        if (msg.IsError)
        {
            Debug.LogErrorFormat("Oculus: Error verifying the user is entitled to the application. Error Message: {0}",
                msg.GetError().Message);
            PerformCallback();
        }
        else
        {
            GetLoggedInUser();
        }
    }

    private void GetLoggedInUser()
    {
        Users.GetLoggedInUser().OnComplete(OnLoggedInUserCallback);
    }

    private void OnLoggedInUserCallback(Message<User> msg)
    {
        if (msg.IsError)
        {
            Debug.LogErrorFormat("Oculus: Error getting logged in user. Error Message: {0}",
                msg.GetError().Message);
            PerformCallback();
        }
        else
        {
            PerformCallback(msg.Data.ID.ToString());
        }
    }

    private void PerformCallback(string oculusId = "")
    {
        callback?.Invoke(oculusId);
        callback = null;
    }
}