using Photon.Pun;
using Photon.Realtime;
using System;
using UnityEngine;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject connectingPanel;
    [SerializeField]
    private DebugPanel debugPanel;
    [SerializeField]
    private NetworkPlayerSpawner playerSpawner;

    private void Start()
    {
        connectingPanel.SetActive(true);
        ConnectToServer();
    }

    private void ConnectToServer()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("Connected to server");
        debugPanel.AddText("Connected to server");

        RoomOptions roomOptions = new();
        roomOptions.MaxPlayers = 4;
        roomOptions.IsVisible = true;
        roomOptions.IsOpen = true;

        PhotonNetwork.JoinOrCreateRoom("Room 1", roomOptions, TypedLobby.Default);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        OculusAuth oculusAuth = new();
        oculusAuth.LogInUser(OnUserLoggedIn);

        Debug.Log("Joined the room");
        debugPanel.AddText("Joined the room");

    }

    private void OnUserLoggedIn(string obj)
    {
        playerSpawner.SpawnPlayer();
        connectingPanel.SetActive(false);
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        Debug.Log("Left the room");
        debugPanel.AddText("User left room");

        playerSpawner.DestroyPlayer();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log($"{newPlayer} joined the room");
        debugPanel.AddText($"{newPlayer} joined the room");
    }
}
