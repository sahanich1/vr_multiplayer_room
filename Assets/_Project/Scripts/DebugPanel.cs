﻿using TMPro;
using UnityEngine;

public class DebugPanel : MonoBehaviour
{
    public TextMeshProUGUI Text;

    public void AddText(string text)
    {
        Text.text += text + "\n";
    }
}
