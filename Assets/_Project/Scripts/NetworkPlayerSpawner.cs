using Oculus.Avatar2;
using Photon.Pun;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;

public class NetworkPlayerSpawner : MonoBehaviour
{
    private const string SpawningPointIdProperty = "SpawningPointId";

    [SerializeField]
    private XROrigin playerOrigin;
    [SerializeField]
    private OvrAvatarEntity playerAvatar;
    [SerializeField]
    private NetworkPlayer networkPlayerPrefab;
    [SerializeField]
    private Transform[] spawningPoints;
    [SerializeField]
    private DebugPanel debugPanel;

    private NetworkPlayer spawnedPlayer;

    private void Start()
    {
        playerOrigin.gameObject.SetActive(false);
    }

    public void SpawnPlayer()
    {
        string logText;

        List<int> freeSpawningPointIDs = new();
        for (int i = 0; i < spawningPoints.Length; i++)
        {
            freeSpawningPointIDs.Add(i);
        }

        foreach (var item in PhotonNetwork.PlayerList)
        {
            if (item == PhotonNetwork.LocalPlayer || item.CustomProperties == null)
            {
                continue;
            }

            if (item.CustomProperties.TryGetValue(SpawningPointIdProperty, out object spawningPointId))
            {
                freeSpawningPointIDs.Remove((int)spawningPointId);
            }
        }

        int chosenSpawningPointId;
        if (freeSpawningPointIDs.Count > 0)
        {
            chosenSpawningPointId = freeSpawningPointIDs[0];           
        }
        else
        {
            logText = "Too many players. Cannot choose spawning point. Setting Random";
            Debug.Log(logText);
            debugPanel.AddText(logText);
            chosenSpawningPointId = UnityEngine.Random.Range(0, spawningPoints.Length);
        }

        Transform spawningPoint = spawningPoints[chosenSpawningPointId];

        spawnedPlayer = PhotonNetwork.Instantiate(networkPlayerPrefab.name, spawningPoint.position, spawningPoint.rotation)
            .GetComponent<NetworkPlayer>();
        spawnedPlayer.Init(playerAvatar);

        logText = $"Spawned in {spawningPoint}";
        Debug.Log(logText);
        debugPanel.AddText(logText);

        ExitGames.Client.Photon.Hashtable properties = PhotonNetwork.LocalPlayer.CustomProperties;
        properties ??= new();
        properties[SpawningPointIdProperty] = chosenSpawningPointId;
        PhotonNetwork.LocalPlayer.SetCustomProperties(properties);

        playerOrigin.gameObject.transform.SetPositionAndRotation(spawningPoint.position, spawningPoint.rotation);
        playerOrigin.MoveCameraToWorldLocation(new Vector3(playerOrigin.transform.position.x, 
            playerOrigin.Camera.transform.position.y, playerOrigin.transform.position.z));
        playerOrigin.MatchOriginUpCameraForward(spawningPoint.up, spawningPoint.forward);
        playerOrigin.gameObject.SetActive(true);
    }

    public void DestroyPlayer()
    {
        if (spawnedPlayer != null)
        {
            PhotonNetwork.Destroy(spawnedPlayer.gameObject);
        }
    }
}